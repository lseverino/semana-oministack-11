import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FiPower, FiTrash2 } from 'react-icons/fi';

import './styles.css';
import logoImg from '../../assets/logo.svg';

import api from '../../services/api';

export default function Profile() {

    const ongId = localStorage.getItem('ongId');
    const ongNome = localStorage.getItem('ongNome');

    const [casos, setCasos] = useState([]);
    const history = useHistory();

    useEffect( () => {
        api.get('/profile', {
            headers: {
                Authorization: ongId,
            }
        }).then(response => {
            console.log("adasdasdsadsad", response.data);
            setCasos(response.data.incidents);
        });
    }, [ongId] ); 

    async function handleDeleteIncident(id) {
        try {

            await api.delete(`incidents/${id}`, {
                headers: { Authorization: ongId, }
            });

            setCasos(casos.filter(caso => caso.id !== id));
            //  alert("Registro excluído com sucesso ! ");
        } catch (error) {
            alert("Erro ao deletar ");
        }
    }

    function handleLogout() {
        localStorage.clear();
        history.push('/');
    }

    return (
        <div className="profile-container">
            <header>
                <img src={logoImg} alt="Be the Hereo" />
                <span>Bem vinda, {ongNome}</span>

                <Link className="button" 
                    to="/incidents/new">Cadastrar novo caso</Link>
                <button type="button" onClick={handleLogout}>
                    <FiPower size="18" color="#E02041" />
                </button>
            </header>

            <h1>Casos cadastrados</h1>
            <ul>
                {casos.map( caso => (
                    <li key={caso.id}>
                    <strong>CASO:</strong>
                    <p>{caso.title}</p>

                    <strong>DESCRIÇÃO:</strong>
                    <p>{caso.description}</p>

                    <strong>VALOR:</strong>
                    <p>{Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL'}).format(caso.value)}</p>

                    <button type="button" onClick={ () => handleDeleteIncident(caso.id) }>
                        <FiTrash2 size="20" color="#a8a8b3" />
                    </button>
                </li>
                ))}
            </ul>
        </div>
    )
}